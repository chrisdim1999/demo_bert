### LogInnov Depth Project

Directories should be as following:
```
loginnov_depth
├── min_depth.py
├── visualized
│   └── ...
├── depth_estimation
│   ├── depth.py
│   └── depth_colored
|   │   └── ...
│   └── disparity_values
|   │   └── ...
│   └── frames
|       └── ...
└── yolov5
    └── detect.py
    └── ...
        └── ...
```

- ```depth.py```

Scripts that uses OAK-D Lite for real time depth estimation. Uses YOLOv5 small trained on CrowdHuman for Object Detection. Saves the frames in directory ```depth_estimation/frames/```, the disparity map values as numpy arrays in directory depth_estimation/disparity_values/ and the colormapped frames of depth values in directory depth_estimation/depth_colored/

Usage: ```python depth.py```

- ```detect.py```

Script of original YOLO repository for inference on given frames (**subRepo**). Must have access to the weights of YOLO model trained on CrowdHuman. Saves labels in directory ```yolov5/runs/detect/exp*/labels```

Usage: ```python detect.py --source ../depth_estimation/frames --weights ../path/to/weights/best.pt --save-txt```

- ```min_depth.py```

Script that returns minimum distance of detected objects for given frames. Must be given access to experiment number of path of latest YOLO experiment/visualization. Saves final depth visualizations in directory ```loginnov_depth/visualized```

Usage: ```python min_depth.py --exp <EXPERIMENT_NUMBER>```
